﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using System.Collections;
using UnityEngine.UI;

public class StartButtonController : MonoBehaviour
{

    [SerializeField] VideoPlayer videoPlayer;
    [SerializeField] GameObject soundEffects;

    private bool startButtonPressed;

    private Animator startbuttonAnimator;
    private AudioSource audioSource;

    private void Awake()
    {
        startbuttonAnimator = GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (InputManager.SubmitButton())
        {
            startbuttonAnimator.SetTrigger("Pressed");
            startButtonPressed = true;
        }
        else
        {
            startButtonPressed = false;
        }
    }

    public void PlayPressedSound()
    {
        audioSource.Play();
        StartCoroutine(TemporarySoundEffectsOff());
        //PlayLevelTransition();
    }

    private IEnumerator TemporarySoundEffectsOff()
    {
        soundEffects.SetActive(false);
        yield return new WaitForSecondsRealtime(1f);
        if (!startButtonPressed)
        {
            soundEffects.SetActive(true);
        }        
    }

    public void StartButtonPressed()
    {
        //PlayLevelTransition();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void PlayLevelTransition()
    {
        foreach (Transform child in transform)
        {
            //child.gameObject.SetActive(false);
        }
        videoPlayer.Play();
        StartCoroutine(LevelTransitionCutscene());
    }

    private IEnumerator LevelTransitionCutscene()
    {
        int frameCount = 60;
        yield return new WaitUntil(() => videoPlayer.frame == frameCount);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
