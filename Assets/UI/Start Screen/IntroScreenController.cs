﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class IntroScreenController : MonoBehaviour
{
    [SerializeField] VideoPlayer videoPlayer;
    [SerializeField] GameObject skipButton;

    private void Start()
    {
        StartCoroutine(IntroTransitionCutscene());
    }

    private void Update()
    {
        if (InputManager.SubmitButton())
        {
            skipButton.SetActive(true);
        }
        if (Input.GetMouseButton(0))
        {
            skipButton.SetActive(true);
        }
    }

    public void SkipButtonPressed()
    {
        GotoNextScene();
    }

    private IEnumerator IntroTransitionCutscene()
    {
        int frameCount = 2500;
        yield return new WaitUntil(() => videoPlayer.frame == frameCount);
        GotoNextScene();
    }

    private void GotoNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
