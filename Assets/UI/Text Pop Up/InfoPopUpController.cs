﻿using UnityEngine;
using System.Collections;

public class InfoPopUpController : MonoBehaviour
{
    [SerializeField] GameObject renderTexture;

    private bool touched;
    private bool popUpManagerActive;

    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        renderTexture.SetActive(false);
        transform.GetChild(0).gameObject.SetActive(false);
    }

    private void LateUpdate()
    {
        bool playerIsTouching =  gameObject.GetComponent<BoxCollider2D>().IsTouchingLayers(LayerMask.GetMask("Player"));

        if (playerIsTouching)
        {
            if (!popUpManagerActive)
            {
                StartCoroutine(InfoBoardPopUpManager());
            }           
            
            touched = true;
        }
        else
        {
            
            touched = false;
        }
    }

    private IEnumerator InfoBoardPopUpManager()
    {
        popUpManagerActive = true;
        _animator.Play("InfoBoardTouched");
        transform.GetChild(0).gameObject.SetActive(true);
        yield return new WaitForSeconds(0.05f);
        renderTexture.SetActive(true);
        yield return new WaitForSeconds(1f);
        if (!touched)
        {
            _animator.Play("InfoBoardIdle");
            renderTexture.SetActive(false);
            transform.GetChild(0).gameObject.SetActive(false);
            popUpManagerActive = false;
        }
        else
        {
            StartCoroutine(InfoBoardPopUpManager());
        }        
    }
}
