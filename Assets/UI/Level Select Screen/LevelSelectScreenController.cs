﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using System.Collections;
using UnityEngine.EventSystems;
using TMPro;

public class LevelSelectScreenController : MonoBehaviour
{

    [SerializeField] VideoPlayer videoPlayer;
    [SerializeField] AudioSource soundEffects;
    [SerializeField] AudioClip selectSound;

    private AudioSource _audioSource;
    private TextMeshProUGUI footerText;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        footerText = transform.GetChild(3).GetComponentInChildren<TextMeshProUGUI>();
    }

    private void Start()
    {
        //Cursor.visible = false;
        //Cursor.lockState = CursorLockMode.Locked;
        
    }

    private void Update()
    {
        if (EventSystem.current.currentSelectedGameObject)
        {
            FooterController();
        }
        
        if (Input.GetButtonDown("Horizontal") || Input.GetButtonDown("Vertical"))
        {
            _audioSource.PlayOneShot(selectSound);
        }
    }

    private void FooterController()
    {
        footerText.text = EventSystem.current.currentSelectedGameObject.gameObject.name.ToString();
    }

    public void GotoLevel(int levelNo)
    {
        soundEffects.Stop();
        foreach(Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
        _audioSource.Play();
        videoPlayer.Play();
        StartCoroutine(LevelTransitionCutscene(levelNo));
    }

    private IEnumerator LevelTransitionCutscene(int levelNo)
    {
        int frameCount = 60;
        yield return new WaitUntil(() => videoPlayer.frame == frameCount);
        levelNo = levelNo + 3;
        SceneManager.LoadScene(levelNo);
    }
}
