﻿using UnityEngine;
using TMPro;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LevelSelectButtonController : Selectable
{

    [SerializeField] Color selectedTextColor;
    private Color defaultColor;

    private TextMeshProUGUI text;

    BaseEventData baseEventData;

    private void Start()
    {
        text = GetComponentInChildren<TextMeshProUGUI>();
        defaultColor = text.color;
        //text.color = selectedTextColor;
    }

    private void Update()
    {
        if (IsHighlighted(baseEventData) == true)
        {
            text.color = selectedTextColor;
        }
        else
        {
            text.color = defaultColor;
        }
    }
}
