﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroTransitionController : MonoBehaviour {

    [SerializeField] RectTransform spiral;

    private void GotoLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void Update()
    {
        spiral.Rotate(Vector3.forward * 8f);
    }
}
