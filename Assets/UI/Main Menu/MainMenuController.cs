﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using System.Collections;

public class MainMenuController : MonoBehaviour {

    [SerializeField] VideoPlayer videoPlayer;
    [SerializeField] GameObject soundEffects;

    private GameObject[] buttons;
    private AudioSource _audioSource;

    public void StartButtonPressed()
    {
        StartCoroutine(StartGame());
    }


    private IEnumerator StartGame()
    {
        soundEffects.SetActive(false);
        _audioSource.Play();
        yield return new WaitForSecondsRealtime(0.5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void ExitButtonPressed()
    {
        Application.Quit();
    }

    private void Awake()
    {
        buttons = new GameObject[2];
        buttons[0] = transform.GetChild(0).GetChild(1).gameObject;
        buttons[1] = transform.GetChild(1).GetChild(1).gameObject;
        buttons[0].SetActive(false);
        buttons[1].SetActive(false);
        _audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {        
        StartCoroutine(IntroductionCutsceneOnFinish());
    }

    private IEnumerator IntroductionCutsceneOnFinish()
    {        
        int frameCount = 235;
        yield return new WaitUntil(() => videoPlayer.frame == frameCount);
        buttons[0].SetActive(true);
        buttons[1].SetActive(true);
    }
}
