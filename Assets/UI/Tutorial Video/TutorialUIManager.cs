﻿using UnityEngine;
using System.Collections;

public class TutorialUIManager : MonoBehaviour
{
    [SerializeField] bool movement;
    [SerializeField] bool mouse;
    [SerializeField] bool mouse2;

    private bool hide;

    private LevelManager levelManager;

    private void Awake()
    {
        levelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
    }

    private void Update()
    {
        if (PauseScreenManager.paused || levelManager.levelComplete)
        {
            if (!hide)
            {
                Hide();
            }            
        }
        else
        {
            if (movement)
            {
                HideOnMovement();
            }
            if (mouse || mouse2)
            {
                HideOnMouseMovement();
            }
        }

        
    }

    private void HideOnMovement()
    {
        float h = InputManager.MainJoystick().x;
        float v = InputManager.MainJoystick().y;

        if (Mathf.Abs(h) > 0.1f || Mathf.Abs(v) > 0.1f)
        {
            if (!hide)
            {
                Hide();
            }
        }
 
    }

    private void HideOnMouseMovement()
    {
        float h = Input.GetAxis("Rightjoystick X");
        float v = Input.GetAxis("Rightjoystick Y");
        Vector2 mouseAxis = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        if (Mathf.Abs(h) > 0.1f || Mathf.Abs(v) > 0.1f || mouseAxis.magnitude > 0.01f)
        {
            if (mouse)
            {
                if (InputManager.ShootBlueOrbButtonDown())
                {
                    if (!hide)
                    {
                        Hide();
                    }
                }
            }
            if (mouse2)
            {
                if (InputManager.ShootPinkOrbButtonDown())
                {
                    if (!hide)
                    {
                        Hide();
                    }
                }
            }    
        }        
    }

    private void Hide()
    {
        hide = true;
        foreach(Transform child in transform)
        {
            child.gameObject.SetActive(false);
        }
    }
}
