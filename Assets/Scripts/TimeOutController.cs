﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TimeOutController : MonoBehaviour
{

    private float timeOutTimer;

    private bool timeOut;

    private void Update()
    {
        if (!Input.anyKey && !timeOut)
        {
            if(GameManager.gameState == GameManager.GameState.Levels)
            {
                StartCoroutine(TimeOutTimer());
                timeOut = true;
            }
            else if(GameManager.gameState == GameManager.GameState.Menus)
            {
                StartCoroutine(LevelSelectScreenTimeOut());
                timeOut = true;
            }
        }

        //Debug.Log(timeOutTimer);
    }

    private IEnumerator LevelSelectScreenTimeOut()
    {
        while (!Input.anyKey)
        {
            timeOutTimer += 1 * Time.unscaledDeltaTime;
            if (timeOutTimer > 30f)
            {
                SceneManager.LoadScene(0);
            }
            yield return null;
        }
        timeOutTimer = 0;
        timeOut = false;
    }

    private IEnumerator TimeOutTimer()
    {
        while (!Input.anyKey)
        {
            timeOutTimer += 1 * Time.unscaledDeltaTime;
            if (timeOutTimer > 30f)
            {
                PauseScreenManager.timeOut = true;
                timeOutTimer = 0;
                StartCoroutine(PauseTimeOutTimer());
            }
            yield return null;
        }
        timeOutTimer = 0;
        timeOut = false;
    }

    private IEnumerator PauseTimeOutTimer()
    {
        while (!Input.anyKey)
        {
            timeOutTimer += 1 * Time.unscaledDeltaTime;
            if (timeOutTimer > 30f)
            {
                PauseScreenManager.RestartGame();
            }
            yield return null;
        }
        timeOutTimer = 0;
        timeOut = false;
    }
}
