﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class OrbGunController : MonoBehaviour
{
    #region Orb Configurations
    [SerializeField] LayerMask conductiveSurfaces;
    private LayerMask blueSurface;
    private LayerMask pinkSurface;
    private Color defualtColor;
    [SerializeField] Color touchingBlueColor;
    [SerializeField] Color touchingPinkColor;
    [SerializeField] GameObject dustEffect;
    Vector2 fireDirection;
    #endregion

    #region flags
    private bool blueOrbUsable;
    private bool pinkOrbUsable;
    #endregion

    #region states
    private enum AimingSystem
    {
        Mouse, 
        Joystick
    }
    private AimingSystem aimingSystem;
    #endregion

    #region Cached Components
    private LevelManager levelManager;
    private AudioSource _audioSource;
    private Transform playerCenter;    
    private GameObject[] orb;
    private Renderer[] _renderer;
    private ParticleSystem[] _orbParticleSystem;
    private GameObject hitPointReticle;
    private CircleCollider2D hitPointCollider;
    private SpriteRenderer hitPointColor;
    private GameObject mouseReticle;

    private List<OrbController> orbController;
    #endregion

    private void Awake()
    {
        #region Cached Components Setup        
        levelManager = GameObject.FindGameObjectWithTag("LevelManager").transform.GetComponent<LevelManager>();
        _audioSource = GetComponentInChildren<AudioSource>();
        blueSurface = LayerMask.GetMask("-");
        pinkSurface = LayerMask.GetMask("+");        
        blueOrbUsable = levelManager.blueOrbUsable;
        pinkOrbUsable = levelManager.pinkOrbUsable;
        hitPointReticle = transform.GetChild(0).gameObject;
        hitPointCollider = hitPointReticle.GetComponent<CircleCollider2D>();
        hitPointColor = hitPointReticle.GetComponent<SpriteRenderer>();
        defualtColor = hitPointColor.color;
        mouseReticle = transform.GetChild(1).gameObject;
        mouseReticle.transform.parent = null;
        Cursor.visible = false;
        orbController = new List<OrbController>();
        SetupOrbs();
        #endregion
    }

    private void SetupOrbs()
    {
        //if (blueOrbUsable || pinkOrbUsable)
        //{
            hitPointReticle.SetActive(true);
            playerCenter = GameObject.FindGameObjectWithTag("Player").transform.GetChild(3).GetComponent<Transform>();
            orb = new GameObject[2];
            _renderer = new Renderer[4];
            _orbParticleSystem = new ParticleSystem[2];
        //}
        if (!blueOrbUsable && !pinkOrbUsable)
        {
            CrosshairUI(false);
        }
        //if (blueOrbUsable)
        //{
            orb[0] = GameObject.FindGameObjectWithTag("BlueOrb");

            _renderer[0] = orb[0].GetComponent<Renderer>();
            _renderer[2] = orb[0].transform.GetChild(0).gameObject.GetComponent<Renderer>();
            _orbParticleSystem[0] = orb[0].transform.GetChild(1).GetComponent<ParticleSystem>();

            orbController.Add(orb[0].GetComponent<OrbController>());
        //}
        //if (pinkOrbUsable)
        //{
            orb[1] = GameObject.FindGameObjectWithTag("PinkOrb");

            _renderer[1] = orb[1].GetComponent<Renderer>();
            _renderer[3] = orb[1].transform.GetChild(0).gameObject.GetComponent<Renderer>();
            _orbParticleSystem[1] = orb[1].transform.GetChild(1).GetComponent<ParticleSystem>();

            orbController.Add(orb[1].GetComponent<OrbController>());
        //}
    }

    private void FixedUpdate()
    {
        blueOrbUsable = levelManager.blueOrbUsable;
        pinkOrbUsable = levelManager.pinkOrbUsable;
        if (blueOrbUsable || pinkOrbUsable)
        {
            ShootController();
            AimDirection();
        }
    }

    private void LateUpdate()
    {
        blueOrbUsable = levelManager.blueOrbUsable;
        pinkOrbUsable = levelManager.pinkOrbUsable;
        if (blueOrbUsable || pinkOrbUsable)
        {
            ShootControllerLate();
            AimJeff();
        }
    }

    public void CrosshairUI(bool state)
    {
        hitPointReticle.SetActive(state);
        mouseReticle.SetActive(false);
    }

    private void AimDirection()
    {
        var playerLookDirection = playerCenter.transform.parent.localScale.x;
        if (playerLookDirection == 1)
        {
            playerCenter.localScale = new Vector2(1f, 1f);
            transform.localScale = new Vector2(1f, 1f);
        }
        if (playerLookDirection == -1)
        {
            playerCenter.localScale = new Vector2(-1f, 1f);
            transform.localScale = new Vector2(-1f, 1f);
        }
    }

    private void AimJeff()
    {        
        //Vector2 direction = Camera.main.ScreenToWorldPoint(Input.mousePosition) - playerCenter.transform.position;
        Vector2 direction = hitPointReticle.transform.position - playerCenter.transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        playerCenter.rotation = Quaternion.Slerp(playerCenter.transform.rotation, rotation, 5f * Time.deltaTime);
    }

    private void ShootControllerLate()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mouseAxis = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        Vector2 rightJoystick = new Vector2(Input.GetAxis("Rightjoystick X"), Input.GetAxis("Rightjoystick Y"));
        //Debug.Log(rightJoystick);
        Vector2 firePointPos = playerCenter.transform.position;
        switch (aimingSystem)
        {
            case AimingSystem.Joystick:
                mouseReticle.SetActive(false);
            
                break;
            case AimingSystem.Mouse:
                mouseReticle.SetActive(true);
                fireDirection = mousePos - firePointPos;
                mouseReticle.transform.position = mousePos;
                
                break;
        }       
        
        //RaycastHit2D hit = Physics2D.Raycast(firePointPos, mousePos - firePointPos, Mathf.Infinity, conductiveSurfaces);
        RaycastHit2D hit = Physics2D.Raycast(firePointPos, fireDirection, Mathf.Infinity, conductiveSurfaces);
        
        hitPointReticle.transform.position = hit.point;

        bool touchingBlue = hitPointCollider.IsTouchingLayers(blueSurface);
        bool touchingPink = hitPointCollider.IsTouchingLayers(pinkSurface);
                
        if (hit.collider != null)
        {
            if (InputManager.ShootBlueOrbButtonDown() || InputManager.ShootPinkOrbButtonDown())
            {
                hitPointReticle.GetComponent<Animator>().SetTrigger("Pressed");
            }
            if (touchingBlue && !touchingPink)
            {                
                hitPointColor.color = touchingPinkColor;
                if (InputManager.ShootBlueOrbButtonDown() && !orbController[0].orbSpawning)
                {
                    ShotBlocked(hit);
                    _audioSource.Play();
                    //orbController[0].PlayBounceAnimation();
                }
            }
            else if (touchingPink && !touchingBlue)
            {                
                hitPointColor.color = touchingBlueColor;
                if (InputManager.ShootPinkOrbButtonDown() && !orbController[1].orbSpawning)
                {
                    ShotBlocked(hit);
                    _audioSource.Play();
                    //orbController[1].PlayBounceAnimation();
                }
            }
            else if (!touchingBlue && !touchingPink)
            {
                hitPointColor.color = defualtColor;                
            }
        }

    }
   
    private void ShootController()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 mouseAxis = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        Vector2 rightJoystick = new Vector2(Input.GetAxis("Rightjoystick X"), Input.GetAxis("Rightjoystick Y"));
        Vector2 firePointPos = playerCenter.transform.position;

        switch (aimingSystem)
        {
            case AimingSystem.Joystick:
                if (rightJoystick.magnitude > 0f)
                {
                    fireDirection = rightJoystick;
                }
                if (mouseAxis.magnitude > 0.3f)
                {
                    aimingSystem = AimingSystem.Mouse;
                }
                break;
            case AimingSystem.Mouse:
                fireDirection = mousePos - firePointPos;
                if (rightJoystick.magnitude > 0.3f)
                {
                    aimingSystem = AimingSystem.Joystick;
                }
                break;
        }

      

        //RaycastHit2D hit = Physics2D.Raycast(firePointPos, mousePos - firePointPos, Mathf.Infinity, conductiveSurfaces);
        RaycastHit2D hit = Physics2D.Raycast(firePointPos, fireDirection, Mathf.Infinity, conductiveSurfaces);

        //reminder - turn "gizmos" 'ON' in game tab to see raycast lines
        //Debug.DrawLine(firePointPos, hit.point);        

        //bool touchingConductiveSurface = hitPointCollider.IsTouchingLayers(conductiveSurfaces);
        bool touchingBlue = hitPointCollider.IsTouchingLayers(blueSurface);
        bool touchingPink = hitPointCollider.IsTouchingLayers(pinkSurface);
        
        if(hit.collider != null)
        {
            if (touchingBlue && !touchingPink)
            {
                if (pinkOrbUsable)
                {
                    ShootPinkOrb(hit);
                }
                if (InputManager.ShootBlueOrbButtonDown() && !orbController[0].orbSpawning)
                {
                    orbController[0].PlayBounceAnimation();
                }
            }
            else if (touchingPink && !touchingBlue)
            {
                if (blueOrbUsable)
                {
                    ShootBlueOrb(hit);                    
                }
                if (InputManager.ShootPinkOrbButtonDown() && !orbController[1].orbSpawning)
                {
                    orbController[1].PlayBounceAnimation();
                }
            }
            else if (!touchingBlue && !touchingPink)
            {
                if (pinkOrbUsable)
                {
                    ShootPinkOrb(hit);
                }
                if (blueOrbUsable)
                {
                    ShootBlueOrb(hit);
                }
            }
        }        
    }

    private void ShotBlocked(RaycastHit2D hit)
    {
        Instantiate(dustEffect, hit.point, Quaternion.identity);
    }

    private void ShootBlueOrb(RaycastHit2D hit)
    {
        if (InputManager.ShootBlueOrbButton())
        {
            if (!orbController[0].orbSpawning)
            {
                orbController[0].PlaySpawnAnimation();                

                orb[0].transform.position = hit.point;

                if (blueOrbUsable && pinkOrbUsable)
                {
                    _renderer[0].sortingOrder = 1;
                    _renderer[1].sortingOrder = 0;
                    _renderer[2].sortingOrder = 1;
                    _renderer[3].sortingOrder = 0;
                }
            }
        }
    }

    private void ShootPinkOrb(RaycastHit2D hit)
    {
        if (InputManager.ShootPinkOrbButton())
        {
            if (!orbController[1].orbSpawning)
            {
                orbController[1].PlaySpawnAnimation();

                orb[1].transform.position = hit.point;

                if(blueOrbUsable && pinkOrbUsable)
                {
                    _renderer[0].sortingOrder = 0;
                    _renderer[1].sortingOrder = 1;
                    _renderer[2].sortingOrder = 0;
                    _renderer[3].sortingOrder = 1;
                }                
            }
        }
    }
}
