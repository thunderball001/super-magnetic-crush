﻿using UnityEngine;

public class CollectableManager : MonoBehaviour
{
    private LevelManager levelManager;
    private Animator _animator;

    [SerializeField] bool playerHasCollectable;

    private void Start()
    {
        levelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
        _animator = GetComponent<Animator>();
    }

    public void CollectableCollected()
    {
        playerHasCollectable = true;
        _animator.SetTrigger("Collected");
    }

    private void DestroyCollectable()
    {
        Destroy(transform.GetChild(0).gameObject);
    }
}
