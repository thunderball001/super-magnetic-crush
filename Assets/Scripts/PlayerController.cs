﻿using Com.LuisPedroFonseca.ProCamera2D;
using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerController : MonoBehaviour
{
    #region Extra Player Configurations
    private float fixedTimeAmount;
    private bool canJump;
    private float graceTime;
    private float jumpBufferTimer;
    #endregion

    #region Player Configurations
    [SerializeField] float multiplier;
    [SerializeField] float accSpeed;      
    [SerializeField] float jumpingStrength;
    [SerializeField] float fallingStrength;
    [SerializeField] float jumpingStrengthClamp;
    [SerializeField] float fallingStrengthClamp;
    [SerializeField] float dashAccSpeed;
    [SerializeField] float dashJumpStrength;
    [SerializeField] float dashFallStrength;
    [SerializeField] Vector2 deathKick;
    [SerializeField] AudioClip[] deathSound;
    [SerializeField] GameObject landDustEffect;
    [SerializeField] AudioClip landSound;
    private bool spawnDustEffect;
    #endregion

    #region Animation States
    [SerializeField] private enum AnimationState
    {
        Idle,
        Running,
        Sliding,
        Jumping,
        Falling,
        RunningDash,
        FallingDash,
        Land
    }
    [SerializeField] private AnimationState animationState;
    #endregion

    #region Cached Components
    private LevelManager levelManager;
    private RigidbodyController rigidbodyController;
    private MagnetCollisionController magnetCollisionController;
    private Rigidbody2D _rb2d;
    private BoxCollider2D _cheek;
    private BoxCollider2D _feet;
    private Vector2 presetFeetOffset;
    private Vector2 presetFeetSize;
    private Animator _animator;
    private float presetAccSpeed;
    private float presetJumpingStrength;
    private float presetFallingStrength;
    private ParticleSystem _footTrailEffect;
    private AudioSource _audioSource;
    [NonSerialized] public GameObject loveHeart;
    #endregion

    #region Death Controller
    [SerializeField] GameObject deathControllerPrefab;
    private DeathController deathController;
    #endregion

    private void Awake()
    {
        gameObject.tag = "Player";
        loveHeart = transform.GetChild(6).gameObject;
        loveHeart.SetActive(false);
        canJump = true;
        jumpBufferTimer = 4f;
        SetupPlayer();
    }

    public void SetupPlayer()
    {
        fixedTimeAmount = Time.fixedDeltaTime * 50f;
        #region Player Configs
        accSpeed = accSpeed * multiplier * fixedTimeAmount;
        jumpingStrength = jumpingStrength * multiplier * fixedTimeAmount;
        fallingStrength = fallingStrength * multiplier * fixedTimeAmount;
        jumpingStrengthClamp = jumpingStrengthClamp * multiplier * fixedTimeAmount;
        fallingStrengthClamp = fallingStrengthClamp * multiplier * fixedTimeAmount;
        dashAccSpeed = dashAccSpeed * multiplier * fixedTimeAmount;
        dashJumpStrength = dashJumpStrength * multiplier * fixedTimeAmount;
        dashFallStrength = dashFallStrength * multiplier * fixedTimeAmount;
        #endregion

        #region Cached Components Setup
        levelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
        rigidbodyController = GetComponent<RigidbodyController>();
        magnetCollisionController = GetComponentInChildren<MagnetCollisionController>();
        _rb2d = GetComponent<Rigidbody2D>();
        _cheek = transform.GetChild(1).GetComponent<BoxCollider2D>();
        _feet = transform.GetChild(2).GetComponent<BoxCollider2D>();
        presetFeetOffset = _feet.offset * fixedTimeAmount;
        presetFeetSize = _feet.size * fixedTimeAmount;
        _animator = GetComponent<Animator>();
        presetAccSpeed = accSpeed * fixedTimeAmount;
        presetJumpingStrength = jumpingStrength * fixedTimeAmount;
        presetFallingStrength = fallingStrength * fixedTimeAmount;
        _footTrailEffect = transform.GetChild(4).GetComponent<ParticleSystem>();
        _audioSource = transform.GetChild(5).GetComponent<AudioSource>();
        #endregion

        #region Death Controller Setup
        Instantiate(deathControllerPrefab, transform);
        deathController = GetComponentInChildren<DeathController>();        
        #endregion
    }

    private void FixedUpdate()
    {
        fixedTimeAmount = Time.fixedDeltaTime * 50f; //50 calls per second
        Accelerate(fixedTimeAmount);
        OnGroundController(fixedTimeAmount);
        InputJumpBuffer();        
        FeetCollisionController(fixedTimeAmount);
        Dash(fixedTimeAmount);
        HazardController(fixedTimeAmount);
    }

    private void LateUpdate()
    {
        FlipSprite(fixedTimeAmount);
        FootTrailEffectController();
        LandEffectController();
        ManageAnimationStates(50f * Time.smoothDeltaTime);
        //ManageAnimationStates(fixedTimeAmount);
    }

    private void Accelerate(float fixedTimeAmount)
    {
        //float h = InputManager.MainHorizontal() * fixedTimeAmount;
        //float h = InputManager.MainJoystick().x * fixedTimeAmount;
        float h = Input.GetAxis("K_MainHorizontal");
        bool onGround = _feet.IsTouchingLayers(LayerMask.GetMask("Ground"));
        bool touchingWall = _cheek.IsTouchingLayers(LayerMask.GetMask("Ground"));
        //bool repelling = _magnetCollisionController.repelling;
        //bool attracting = _magnetCollisionController.attracting;
        if (onGround)
        {
            if (!touchingWall)
            {
                _rb2d.velocity = new Vector2(h * (accSpeed / 1.2f) * fixedTimeAmount, _rb2d.velocity.y);
            }
            else if(touchingWall)
            {
                _rb2d.velocity = new Vector2(h * (accSpeed / 1.4f) * fixedTimeAmount, _rb2d.velocity.y);
            }            
        }
        else if(!onGround)
        {
            if (!touchingWall)
            {
                _rb2d.velocity = new Vector2(h * accSpeed * fixedTimeAmount, _rb2d.velocity.y);
            }
            else if(touchingWall)
            {
                _rb2d.velocity = new Vector2(h * (accSpeed / 1.2f) * fixedTimeAmount, _rb2d.velocity.y);
            }
        }      
    }

    private void OnGroundController(float fixedTimeAmount)
    {       
        bool onGround = _feet.IsTouchingLayers(LayerMask.GetMask("Ground"));
        
        if (!onGround)
        {
            InAirController(fixedTimeAmount);
            LedgeAssistance(fixedTimeAmount);
            //cannot jump
            return;            
        }
        //else on ground
        _rb2d.velocity = new Vector2(_rb2d.velocity.x, 0f * fixedTimeAmount);
        graceTime = 0f * fixedTimeAmount;
        //and can jump         
        bool jumpFromGround = Input.GetButton("Jump") && canJump && _rb2d.velocity.y <= 0f * fixedTimeAmount;
        if (jumpFromGround)
        {
            Jump(fixedTimeAmount);
        }   
    }

    private void Jump(float fixedTimeAmount)
    {        
        _rb2d.velocity = new Vector2(_rb2d.velocity.x, jumpingStrength * fixedTimeAmount);
    }

    private void InputJumpBuffer()
    {
        //Debug.Log(jumpBufferTimer);
        if (jumpBufferTimer < 4f * fixedTimeAmount)
        {
            canJump = true;
        }
        else
        {
            canJump = false;
        }
        jumpBufferTimer += 0.5f * fixedTimeAmount;
        //Debug.Log(canJump);
        if (Input.GetButton("Jump"))
        {
            jumpBufferTimer = 4f * fixedTimeAmount;
            //canJump = false;
        }
        else
        {
            //canJump = true;
            jumpBufferTimer = 0f * fixedTimeAmount;
        }
    }

    private void LedgeAssistance(float fixedTimeAmount)
    {
        bool touchingWall = _cheek.IsTouchingLayers(LayerMask.GetMask("Ground"));
        //Debug.Log(graceTime); 
        if(graceTime < 3f * fixedTimeAmount)
        {
            graceTime += 0.5f * fixedTimeAmount;
            bool jumpInGraceTime = Input.GetButton("Jump") && canJump && !touchingWall;
            if (jumpInGraceTime)
            {
                Jump(fixedTimeAmount);
            }
        }       
    }

    private void InAirController(float fixedTimeAmount)
    {
        #region Falling Controller
        bool falling = _rb2d.velocity.y < 0 * fixedTimeAmount;        
        if (falling)
        {
            _rb2d.velocity += Vector2.up * Physics2D.gravity.y * (fallingStrength - 1) * Time.fixedDeltaTime;
        }
        #endregion

        #region Holding Jump Button Controller //the longer the player holds the button, the longer they jump
        bool jumpingUpwards = _rb2d.velocity.y > 0 * fixedTimeAmount;
        bool holdingJumpButton = Input.GetButton("Jump");
        if (jumpingUpwards && !holdingJumpButton)
        {
            _rb2d.velocity += Vector2.up * Physics2D.gravity.y * (fallingStrength - 1) * Time.fixedDeltaTime;
        }
        #endregion

        #region Falling Strengh Clamp //artifical clamp to stop the player from falling at unnatural speeds
        if (_rb2d.velocity.y < -fallingStrengthClamp * fixedTimeAmount)
        {
            _rb2d.velocity = new Vector2(_rb2d.velocity.x, -fallingStrengthClamp * fixedTimeAmount);
        }
        #endregion

        #region Jumping Strength Clamp //artifical clamp to stop the player from jumping upwards at unnatural speeds
        if (_rb2d.velocity.y > jumpingStrengthClamp * fixedTimeAmount)
        {
            _rb2d.velocity = new Vector2(_rb2d.velocity.x, jumpingStrengthClamp * fixedTimeAmount);
        }
        #endregion

    }

    private void FeetCollisionController(float fixedTimeAmount)
    {
        bool onGround = _feet.IsTouchingLayers(LayerMask.GetMask("Ground"));
        bool horizontalButtonPressed = Input.GetButton("Horizontal");
        Vector2 movingFeetOffset = new Vector2(presetFeetOffset.x - 0.075f, presetFeetOffset.y);
        Vector2 movingFeetSize = new Vector2(presetFeetSize.x + 0.15f, presetFeetSize.y);
        if (horizontalButtonPressed && onGround)
        {
            _feet.offset = movingFeetOffset * fixedTimeAmount;
            _feet.size = movingFeetSize * fixedTimeAmount;
        }
        else
        {
            _feet.offset = presetFeetOffset * fixedTimeAmount;
            _feet.size = presetFeetSize * fixedTimeAmount;
        }        
    }

    private void Dash(float fixedTimeAmount)
    {        
        bool dash = Input.GetButton("Dash");
        bool falling = _rb2d.velocity.y < 0 * fixedTimeAmount;
        bool jump = Input.GetButton("Jump");
        if (!dash)
        {
            rigidbodyController.DecreaseDrag(0.1f * fixedTimeAmount);
            accSpeed = dashAccSpeed * fixedTimeAmount;
            jumpingStrength = dashJumpStrength * fixedTimeAmount;
            if (falling)
            {
                if (jump)
                {
                    fallingStrength = dashFallStrength * fixedTimeAmount;
                }
                else
                {
                    fallingStrength = (presetFallingStrength / 1.4f) * fixedTimeAmount;
                }
            }
            else
            {
                fallingStrength = dashFallStrength * fixedTimeAmount;
            }
       
        }
        else
        {
            rigidbodyController.IncreaseDrag(0.1f * fixedTimeAmount);
            accSpeed = presetAccSpeed * fixedTimeAmount;
            jumpingStrength = presetJumpingStrength * fixedTimeAmount;
            fallingStrength = presetFallingStrength * fixedTimeAmount;
        }
    }  

    private void FlipSprite(float fixedTimeAmount)
    {
        //float h = InputManager.MainHorizontal() * fixedTimeAmount;
        float h = InputManager.MainJoystick().x * fixedTimeAmount;
        //float h = Input.GetAxis("K_MainHorizontal");
        bool horizontalAxisHasChanged = Mathf.Abs(h) > Mathf.Epsilon;
        if (horizontalAxisHasChanged)
        {
            transform.localScale = new Vector2(Mathf.Sign(h), transform.localScale.y);
        }
    }

    private void FootTrailEffectController()
    {
        bool touchingOtherMagnetCollider = magnetCollisionController.repelling || magnetCollisionController.attracting;
        bool feetTouchingMagnetSurface = _feet.IsTouchingLayers(LayerMask.GetMask("-")) || _feet.IsTouchingLayers(LayerMask.GetMask("+"));
        var emission = _footTrailEffect.emission;
        if (!touchingOtherMagnetCollider && !feetTouchingMagnetSurface)
        {
            emission.enabled = false;
            return;
        }       
        emission.enabled = true;

    }

    private void LandEffectController()
    {
        bool onGround = _feet.IsTouchingLayers(LayerMask.GetMask("Ground"));
        bool repelling = magnetCollisionController.repelling;
        bool attracting = magnetCollisionController.attracting;

        if (onGround || repelling || attracting)
        {
            if (spawnDustEffect)
            {
                ProCamera2DShake.Instance.Shake("GroundShake");
                #region Spawn Dust Effect
                Instantiate(landDustEffect, transform.position, Quaternion.identity);
                spawnDustEffect = false;
                #endregion
                #region Play Sound Effect
                _audioSource.PlayOneShot(landSound);
                #endregion
            }
        }
        else
        {
            spawnDustEffect = true;
        }
    }

    private void OnBecameInvisible()
    {
        if(levelManager != null)
        {
            levelManager.playerOutOfBounds = true;                      
        }              
    }

    private void OnBecameVisible()
    {
        if (levelManager != null)
        {
            levelManager.playerOutOfBounds = false;
        }
    }

    private void HazardController(float fixedTimeAmount)
    {
        bool touchingHazard = gameObject.GetComponent<CapsuleCollider2D>().IsTouchingLayers(LayerMask.GetMask("Hazard"));

        if (touchingHazard)
        {
            PlayDeath();
        }
    }

    public void PlayDeath()
    {
        Instantiate(landDustEffect, transform.position, Quaternion.identity);
        spawnDustEffect = false;
        int randomDeathSound = Random.Range(0, deathSound.Length);
        deathController.PlayDeath(deathKick + _rb2d.velocity * fixedTimeAmount, deathSound[randomDeathSound]);
        gameObject.transform.GetChild(5).parent = null; // jeff follow position            
        levelManager.playerAlive = false;
    }

    private void ManageAnimationStates(float fixedTimeAmount)
    {
        //Debug.Log(animationState);
        #region Animation Speed Controller
        //float h = InputManager.MainHorizontal() * fixedTimeAmount;
        float h = InputManager.MainJoystick().x * fixedTimeAmount;
        //float h = Input.GetAxis("K_MainHorizontal");
        _animator.speed = 1f + Mathf.Abs(h) * fixedTimeAmount;
        #endregion

        #region State Change Parameters
        bool onGround = _feet.IsTouchingLayers(LayerMask.GetMask("Ground"));
        bool touchingWall = _cheek.IsTouchingLayers(LayerMask.GetMask("Ground"));
        bool horizontalButtonPressed = InputManager.MainHorizontal() > 0.1f * fixedTimeAmount || InputManager.MainHorizontal() < -0.1f * fixedTimeAmount;
        bool movingHorizontally = Mathf.Abs(_rb2d.velocity.x * fixedTimeAmount) > Mathf.Epsilon;
        bool jump = Input.GetButton("Jump");
        bool jumpingUpwards = _rb2d.velocity.y > 0 * fixedTimeAmount;
        bool falling = _rb2d.velocity.y < 0 * fixedTimeAmount;
        bool dash = Input.GetButton("Dash");
        bool terminalVelocity = _rb2d.velocity.y < 0.4f * fixedTimeAmount && _rb2d.velocity.y > -0.4f * fixedTimeAmount; 
        float animationTime = _animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
        #endregion

        switch (animationState)
        {
            #region Idle State
            case AnimationState.Idle:
                _animator.Play("Idle");
                if (horizontalButtonPressed && onGround)
                {
                    animationState = AnimationState.Running;
                }
                if (!horizontalButtonPressed && movingHorizontally && onGround)
                {
                    animationState = AnimationState.Sliding;
                }
                if (jump && canJump)
                {
                    animationState = AnimationState.Jumping;
                }               
                if (!onGround && falling)
                {
                    animationState = AnimationState.Falling;
                }
                break;
            #endregion

            #region Running State
            case AnimationState.Running:
                _animator.Play("Running");                
                if (!horizontalButtonPressed)
                {
                    animationState = AnimationState.Sliding;
                }
                if ((jump && !onGround) || (touchingWall && !onGround))
                {
                    animationState = AnimationState.Jumping;
                }                
                if (!onGround && falling)
                {
                    animationState = AnimationState.Falling;
                }                
                if (dash)
                {
                    animationState = AnimationState.RunningDash;
                }
                break;
            #endregion

            #region Sliding State
            case AnimationState.Sliding:
                _animator.Play("Sliding");
                if (!movingHorizontally && animationTime > 0.25f)
                {
                    animationState = AnimationState.Idle;
                }
                if (horizontalButtonPressed && onGround)
                {
                    animationState = AnimationState.Running;
                }
                if (jump && !onGround)
                {
                    animationState = AnimationState.Jumping;
                }                
                if (!onGround && falling)
                {
                    animationState = AnimationState.Falling;
                }
                if(horizontalButtonPressed && dash)
                {
                    animationState = AnimationState.RunningDash;
                }
                break;
            #endregion

            #region Jumping State
            case AnimationState.Jumping:
                _animator.Play("Jumping");
                if (!onGround && falling && !terminalVelocity)
                {
                    animationState = AnimationState.Falling;
                }
                if (onGround && !movingHorizontally && !jump)
                {
                    animationState = AnimationState.Idle;
                }
                if (onGround && horizontalButtonPressed && movingHorizontally && !jump)
                {
                    animationState = AnimationState.Running;
                }
                break;
            #endregion

            #region Falling State
            case AnimationState.Falling:
                _animator.Play("Falling");
                if (onGround && !jumpingUpwards)
                {
                    animationState = AnimationState.Land;
                }                
                if (dash)
                {
                    animationState = AnimationState.FallingDash;
                }
                if (!onGround && jumpingUpwards)
                {
                    animationState = AnimationState.Jumping;
                }
                break;
            #endregion

            #region Dashing Run State
            case AnimationState.RunningDash:
                _animator.Play("Running Dash");
                if (!horizontalButtonPressed)
                {
                    animationState = AnimationState.Idle;
                }
                if (jump && !onGround)
                {
                    animationState = AnimationState.Jumping;
                }                
                if (!onGround && falling)
                {
                    animationState = AnimationState.Falling;
                }
                if (!dash)
                {
                    animationState = AnimationState.Running;
                }
                break;
            #endregion

            #region Dashing Fall State
            case AnimationState.FallingDash:
                _animator.Play("Falling Dash");
                if (onGround && !jumpingUpwards)
                {
                    animationState = AnimationState.Land;
                }
                if (!dash)
                {
                    animationState = AnimationState.Falling;
                }
                if (!onGround && jumpingUpwards)
                {
                    animationState = AnimationState.Jumping;
                }
                break;
            #endregion

            #region Land State
            case AnimationState.Land:                
                _animator.Play("Land");
                if (animationTime > 2f)
                {
                    animationState = AnimationState.Idle;
                }
                if (!onGround && jumpingUpwards)
                {
                    animationState = AnimationState.Jumping;
                }                
                break;
            #endregion
        }
    }
}
