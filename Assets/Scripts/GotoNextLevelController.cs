﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GotoNextLevelController : MonoBehaviour
{
    private LevelTransitionController levelTransitionController;
    private GotoNextLevelController gotoNextLevelController;

    private AudioSource selectSound;
        

    private void Awake()
    {
        levelTransitionController = GetComponent<LevelTransitionController>();
        gotoNextLevelController = GetComponent<GotoNextLevelController>();
        selectSound = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (InputManager.SubmitButton())
        {            
            levelTransitionController.PlayExitAnimation();
            selectSound.Play();
        }
    }    

    private IEnumerator GotoNextLevel()
    {
        gotoNextLevelController.enabled = false;
        yield return new WaitForSeconds(0.1f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        levelTransitionController.PlayerEnterAnimation();       
    }
}
