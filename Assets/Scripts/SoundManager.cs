﻿using UnityEngine;

public class SoundManager : MonoBehaviour {

    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        DontDestroyOnLoad(gameObject);
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if (PauseScreenManager.paused)
        {
            _audioSource.volume = 0f;
        }
        else
        {
            if (PauseScreenManager.resume)
            {
                _audioSource.volume = 1f;
            }            
        }
    }
}
