﻿using UnityEngine;

public class RigidbodyController : MonoBehaviour {

    private float fixedTimeAmount;

    [SerializeField] float multiplier;
    public float minDragClamp;
    public float maxDragClamp;
    public float dashMinDragClamp;
    public float dashMaxDragClamp;

    private Rigidbody2D _rb2d;
    
    public enum RigidBodyState
    {
        Default,
        Magnetised
    }
    public RigidBodyState rigidBodyState;

    private void Awake()
    {
        fixedTimeAmount = Time.fixedDeltaTime * 50f;        
        minDragClamp = minDragClamp * multiplier * fixedTimeAmount;
        maxDragClamp = maxDragClamp * multiplier * fixedTimeAmount;
        dashMinDragClamp = dashMinDragClamp * multiplier * fixedTimeAmount;
        dashMaxDragClamp = dashMaxDragClamp * multiplier * fixedTimeAmount;

        _rb2d = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        fixedTimeAmount = Time.fixedDeltaTime * 50f;
        ManageRigidBodyStates(fixedTimeAmount);
    }

    private void ManageRigidBodyStates(float fixedTimeAmount)
    {
        switch (rigidBodyState)
        {
            case RigidBodyState.Default:
                
                if (_rb2d.drag < minDragClamp * fixedTimeAmount)
                {
                    _rb2d.drag = minDragClamp * fixedTimeAmount;
                }

                if (_rb2d.drag > maxDragClamp * fixedTimeAmount)
                {
                    _rb2d.drag = maxDragClamp * fixedTimeAmount;
                }
                
                break;
            case RigidBodyState.Magnetised:
                
                if (_rb2d.drag < dashMinDragClamp * fixedTimeAmount)
                {
                    _rb2d.drag = dashMinDragClamp * fixedTimeAmount;
                }

                if (_rb2d.drag > dashMaxDragClamp * fixedTimeAmount)
                {
                    _rb2d.drag = dashMaxDragClamp * fixedTimeAmount;
                }     
                
                break;               
        }
    }

    public void ChangeRigidBodyState(RigidBodyState state)
    {
        rigidBodyState = state;
    }

    public void IncreaseDrag(float amount)
    {
        _rb2d.drag += amount * fixedTimeAmount;
    }

    public void DecreaseDrag(float amount)
    {
        _rb2d.drag += -amount * fixedTimeAmount;
    }
}
