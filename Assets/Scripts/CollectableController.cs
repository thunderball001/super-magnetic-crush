﻿using UnityEngine;

public class CollectableController : MonoBehaviour
{
    private CollectableManager collectableManager;
    private AudioSource _audioSource;

    private void Start()
    {        
        collectableManager = GetComponentInParent<CollectableManager>();
        _audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {            
            collectableManager.CollectableCollected();
            _audioSource.Play();
        }
    }    
}
