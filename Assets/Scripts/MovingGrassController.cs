﻿using UnityEngine;

public class MovingGrassController : MonoBehaviour {

    private Animator _animator;
    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _animator = GetComponentInChildren<Animator>();
        _spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        _spriteRenderer.flipX = Random.value < 0.5;
        PresetSpriteAlpha();
    }

    private void PresetSpriteAlpha()
    {
        Color color = _spriteRenderer.color;
        if(gameObject.layer == LayerMask.NameToLayer("Background_0"))
        {
            _spriteRenderer.color = new Color(color.r, color.g, color.b, 0.75f);
        }        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") || collision.CompareTag("Jeff"))
        {
            _animator.SetTrigger("Move Grass");
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("BlueOrb"))
        {
            _animator.SetTrigger("Move Grass");
        }
    }
}
