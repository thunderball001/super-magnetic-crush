﻿using UnityEngine;
using System.Collections;

public class DeathController : MonoBehaviour
{

    public void PlayDeath(Vector2 deathKick, AudioClip deathSound)
    {
        SetActiveAllChildren(transform, true, deathKick);
        GetComponent<AudioSource>().PlayOneShot(deathSound);
        transform.parent = null;
        StartCoroutine(Corpse());
        DontDestroyOnLoad(gameObject);
    }

    private void SetActiveAllChildren(Transform transform, bool value, Vector2 deathKick)
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(value);
            child.GetComponent<Rigidbody2D>().velocity = deathKick;
            SetActiveAllChildren(child, value, deathKick);
        }
    }

    private IEnumerator Corpse()
    {
        foreach (Transform child in transform)
        {
            child.gameObject.GetComponent<SpriteRenderer>().color = new Color(0f, 0f, 0f, 0.5f);            
        }
        yield return new WaitForSecondsRealtime(1f);
        StartCoroutine(Decay());
    }

    private IEnumerator Decay()
    {
        bool decaying = true;
        while (decaying)
        {
            foreach (Transform child in transform)
            {                
                child.gameObject.GetComponent<SpriteRenderer>().color -= new Color(0f, 0f, 0f, 0.01f);
                Color color = child.gameObject.GetComponent<SpriteRenderer>().color;
                if (color.a < 0.01f)
                {
                    Destroy(gameObject);
                }
            }
            
            
            yield return null;
        }       
    }    
}
