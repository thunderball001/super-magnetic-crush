﻿using Com.LuisPedroFonseca.ProCamera2D;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public bool playerAlive;
    public bool playerOutOfBounds;
    public bool blueOrbUsable;
    public bool pinkOrbUsable;
    public bool levelComplete;
    [SerializeField] bool timerOff;
    [SerializeField] bool cinematicPlaying;
    [SerializeField] bool respawning;
 
    private GameObject player;
    private GameObject jeff;

    private RankingManager rankingManager;
    private LevelTransitionController levelTransitionController;
    private ProCamera2DCinematics cinematics;

    private void Awake()
    {
        gameObject.tag = "LevelManager";
        levelTransitionController = GameObject.FindGameObjectWithTag("LevelTransitionController").GetComponent<LevelTransitionController>();
        SetupLevel();
    }

    private void SetupLevel()
    {       
        rankingManager = GameObject.FindGameObjectWithTag("RankingManager").GetComponent<RankingManager>();
        
        cinematics = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<ProCamera2DCinematics>();             

        #region Player Cached Components
        player = GameObject.FindGameObjectWithTag("Player");
        jeff = GameObject.FindGameObjectWithTag("Jeff");
        playerAlive = true;
        #endregion

        StartCoroutine(SetupGame());

        #region FPS Lock Settings //OFF
        //QualitySettings.vSyncCount = 0;
        //Application.targetFrameRate = 60;
        #endregion
    }

    private IEnumerator SetupGame()
    {
        if (timerOff)
        {
            rankingManager.HideTimer();
        }
        //DeactivatePlayer(player.GetComponent<Rigidbody2D>());
        //jeff.SetActive(false);
        //player.SetActive(false);
        yield return new WaitForSeconds(0.2f);
        //jeff.SetActive(true);
        //player.SetActive(true);
        //ActivatePlayer(player.GetComponent<Rigidbody2D>());        
        if(!timerOff)
        {            
            rankingManager.StartLevelTimer();
        }        
    }

    private void DeactivatePlayer(Rigidbody2D playerRigidBody)
    {
        playerRigidBody.velocity = new Vector2(0f, 0f);
        player.GetComponent<PlayerController>().enabled = false;
    }

    private void ActivatePlayer(Rigidbody2D playerRigidBody)
    {
        player.GetComponent<PlayerController>().enabled = true;
        playerRigidBody.velocity = new Vector2(0f, 0f);        
    }    

    private void Update()
    {
        if (playerOutOfBounds && !cinematicPlaying)
        {
            StartCoroutine(PlayerOutOfBoundsController());
        }
        if (!playerAlive)
        {
            StartCoroutine(PlayerDeathController());
        }
        if (PauseScreenManager.paused)
        {
            rankingManager.HideTimer();
        }
        if(!PauseScreenManager.paused && !timerOff)
        {
            rankingManager.DisplayTimer();
        }
    }

    private IEnumerator PlayerOutOfBoundsController()
    {
        yield return new WaitForSecondsRealtime(1f);
        if (playerOutOfBounds && !respawning)
        {
            StartCoroutine(Respawn());
        }        
    }

    private IEnumerator PlayerDeathController()
    {        
        player.SetActive(false);
        yield return new WaitForSecondsRealtime(1f);
        if (!playerAlive && !respawning)
        {
            StartCoroutine(Respawn());
        }        
    }

    private IEnumerator Respawn()
    {
        player.GetComponent<PlayerController>().PlayDeath();
        respawning = true;
        ProCamera2DTransitionsFX.Instance.TransitionExit();
        yield return new WaitForSecondsRealtime(0.5f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public IEnumerator LevelCompleteController()
    {
        //ProCamera2DTransitionsFX.Instance.TransitionExit();
        levelTransitionController.PlayerOutroAnimation();
        timerOff = true;
        rankingManager.HideTimer();
        cinematics.Play();
        levelComplete = true;
        PlayerCelebrate();
        yield return new WaitForSecondsRealtime(0.25f);
        rankingManager.ProcessPlayerRating();
        rankingManager.DisplayEndOfLevelCommands();
        EnableGotoNextLevelController();
    }

    private void PlayerCelebrate()
    {
        player.GetComponent<Animator>().Play("Celebrating");
        player.GetComponent<Animator>().speed = 1f;
        player.GetComponent<PlayerController>().loveHeart.SetActive(true);
        DeactivatePlayer(player.GetComponent<Rigidbody2D>());
    }

    private void EnableGotoNextLevelController()
    {        
        levelTransitionController.EnableGotoNextLevelController();
    }

    public void CinematicPlaying()
    {
        cinematicPlaying = true;
    }

    public void CinematicStopped()
    {
        cinematicPlaying = false;        
    }
}
