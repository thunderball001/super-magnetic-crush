﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DoorwayController : MonoBehaviour
{

    [SerializeField] Scene targetScene;

    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    private void LateUpdate()
    {
        bool playerIsTouching = gameObject.GetComponent<BoxCollider2D>().IsTouchingLayers(LayerMask.GetMask("Player"));

        if (playerIsTouching)
        {
            _animator.Play("DoorOpen");
            if (InputManager.SubmitButton())
            {
                GotoStage();
            }
        }
        else
        {
            _animator.Play("DoorClosed");
        }
    }

    private void GotoStage()
    {
        //SceneManager.LoadScene(targetScene);
    }
}
