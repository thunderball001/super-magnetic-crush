﻿using UnityEngine;
using System;

public class MagnetCollisionController : MonoBehaviour
{
    public enum MagneticCharge
    {
        Negative,
        Positive, 
        Off
    }
    public MagneticCharge magneticCharge;

    [NonSerialized] public bool repelling;
    [NonSerialized] public bool attracting;

    private MagnetPhysicsController magnetPhysicsController;

    private void Awake()
    {
        if(magneticCharge == MagneticCharge.Negative)
        {
            tag = "-";
            gameObject.layer = LayerMask.NameToLayer("-");
        }
        if (magneticCharge == MagneticCharge.Positive)
        {
            tag = "+";
            gameObject.layer = LayerMask.NameToLayer("+");
        }
    }

    private void Start()
    {
        magnetPhysicsController = gameObject.GetComponentInParent<MagnetPhysicsController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("-"))
        {
            if(magneticCharge == MagneticCharge.Negative)
            {
                repelling = true;
            }
            if (magneticCharge == MagneticCharge.Positive)
            {
                attracting = true;
            }
            magnetPhysicsController.negativeMagnetObjects.Add(collision.gameObject);
        }
        if (collision.CompareTag("+"))
        {
            if(magneticCharge == MagneticCharge.Negative)
            {
                attracting = true;
            }
            if (magneticCharge == MagneticCharge.Positive)
            {
                repelling = true;
            }
            magnetPhysicsController.positiveMagnetObjects.Add(collision.gameObject);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("-"))
        {
            if (magneticCharge == MagneticCharge.Negative)
            {
                repelling = false;
            }
            if (magneticCharge == MagneticCharge.Positive)
            {
                attracting = false;
            }
            magnetPhysicsController.negativeMagnetObjects.Remove(collision.gameObject);
        }
        if (collision.CompareTag("+"))
        {
            if (magneticCharge == MagneticCharge.Negative)
            {
                attracting = false;
            }
            if (magneticCharge == MagneticCharge.Positive)
            {
                repelling = false;
            }
            magnetPhysicsController.positiveMagnetObjects.Remove(collision.gameObject);
        }
    }
}
