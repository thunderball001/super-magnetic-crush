﻿using UnityEngine;

public class CameraTargetController : MonoBehaviour
{
    public bool followPlayer;

    private Transform playerTransform;
    private Rigidbody2D playerRb2d;

    private float xOffset;
    private float yOffset;

    private void Awake()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        playerRb2d = playerTransform.GetComponent<Rigidbody2D>();
        transform.position = playerTransform.position;
    }

    private void LateUpdate()
    {
        if (followPlayer)
        {
            FollowPlayer();
        }
    }

    private void FollowPlayer()
    {              
        if (playerRb2d.velocity.x > 3f)
        {
            xOffset += 0.1f;  
        }
        if (playerRb2d.velocity.x < -3f)
        {
            xOffset += -0.1f;
        }
        
        if (playerRb2d.velocity.y > 3f)
        {
            yOffset += 0.1f;
        }
        if (playerRb2d.velocity.y < -3f)
        {
            yOffset += -0.1f;
        }

        if(playerRb2d.velocity.x == 0f && playerRb2d.velocity.y == 0f)
        {
            xOffset = 0f;
            yOffset = 0f;
        }

        xOffset = Mathf.Clamp(xOffset, -3f, 3f);
        yOffset = Mathf.Clamp(yOffset, -3f, 3f);

        transform.position = new Vector2(playerTransform.position.x + xOffset, playerTransform.position.y + yOffset);
    }
}
