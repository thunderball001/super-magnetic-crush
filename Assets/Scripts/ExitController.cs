﻿using UnityEngine;
using System.Collections;
using Com.LuisPedroFonseca.ProCamera2D;

public class ExitController : MonoBehaviour
{
    [SerializeField] AudioClip[] celebrateSounds;

    private LevelManager levelManager;
    private Transform player;

    private Animator _animator;
    private GameObject loveHeart;
    private AudioSource voices;
    private AudioSource music;

    private void Awake()
    {
        loveHeart = transform.GetChild(0).gameObject;
        loveHeart.SetActive(false);

        levelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        FacePlayer();

        _animator = GetComponent<Animator>();
        voices = GetComponent<AudioSource>();
        music = transform.GetChild(1).GetComponentInChildren<AudioSource>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ProCamera2DShake.Instance.StopConstantShaking();
            StartCoroutine(PlayCelebrateMusic());
            _animator.Play("Saved");
            loveHeart.SetActive(true);
            StartCoroutine(levelManager.LevelCompleteController());
            FacePlayer();
            StartCoroutine(FixPlayerPosition(0.75f));
        }
    }

    public IEnumerator PlayCelebrateMusic()
    {
        music.Play();
        yield return new WaitForSecondsRealtime(0.5f);
        voices.PlayOneShot(celebrateSounds[Random.Range(0, 2)]);
        yield return new WaitForSecondsRealtime(0.5f);
        voices.PlayOneShot(celebrateSounds[Random.Range(0, 2)]);
    }

    private void FacePlayer()
    {
        if(player.position.x > transform.position.x)
        {
            transform.localScale = new Vector2(1f, transform.localScale.y);
            player.localScale = new Vector2(-1f, transform.localScale.y);
        }
        else
        {
            transform.localScale = new Vector2(-1f, transform.localScale.y);
            player.localScale = new Vector2(1f, transform.localScale.y);
        }        
    }

    private IEnumerator FixPlayerPosition(float offset)
    {
        while (levelManager.levelComplete)
        {
            if (player.position.x < transform.position.x + offset && player.position.x > transform.position.x)
            {
                player.position = new Vector2(transform.position.x + offset, player.position.y);
            }
            else if(player.position.x > transform.position.x - offset && player.position.x < transform.position.x)
            {
                player.position = new Vector2(transform.position.x - offset, player.position.y);
            }
            yield return null;
        }
    }
}
