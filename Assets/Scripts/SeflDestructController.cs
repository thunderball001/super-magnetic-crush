﻿using UnityEngine;

public class SeflDestructController : MonoBehaviour {

    [SerializeField] float timeTillSelfDestruct;

    private void Start()
    {
        Destroy(gameObject, timeTillSelfDestruct);
    }
}
