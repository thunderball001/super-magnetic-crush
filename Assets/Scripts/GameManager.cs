﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    float deltaTime = 0.0f;

    public enum GameState
    {
        Menus,
        Levels
    }
    public static GameState gameState;

    // called zero
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
        GameStateManager();
    }

    // called first
    void OnEnable()
    {
        //Debug.Log("OnEnable called");
        //SceneManager.sceneLoaded += OnSceneLoaded;
    }

    // called second
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        //Debug.Log("OnSceneLoaded: " + scene.name);
        //Debug.Log(mode);
    }

    // called third
    void Start()
    {
        //Debug.Log("Start");
    }

    private void Update()
    {
        GameStateManager();
        //FrameRateCounter();
    }

    private void FrameRateCounter()
    {
        deltaTime += (Time.unscaledDeltaTime - deltaTime) * 0.1f;
    }

    void OnGUI()
    {
        /*
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(0, 0, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
        GUI.Label(rect, text, style);
        */
    }

    private void GameStateManager()
    {
        //Debug.Log(gameState);
        switch (gameState)
        {
            case GameState.Menus:
                if (SceneManager.GetActiveScene().buildIndex > 2)
                {
                    gameState = GameState.Levels;
                }
                break;
            case GameState.Levels:
                if (SceneManager.GetActiveScene().buildIndex <= 2)
                {
                    gameState = GameState.Menus;
                }
                break;
        }
    }

    // called when the game is terminated
    void OnDisable()
    {
        //Debug.Log("OnDisable");
        //SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}
