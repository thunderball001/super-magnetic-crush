﻿using UnityEngine;

public class JeffController : MonoBehaviour {

    [SerializeField] Transform[] target;

    private enum FollowState
    {
        player,
        orb
    }
    private FollowState followState;

    [SerializeField] float followSpeed;
    private float defaultFollowSpeed;
    [SerializeField] bool followTarget;

    [SerializeField] enum OrbType
    {
        blue,
        pink
    }
    [SerializeField] OrbType orbType;

    private LevelManager levelManager;
    private OrbGunController orbGunController;

    private Animator _animator;

    private void Awake()
    {
        levelManager = GameObject.FindGameObjectWithTag("LevelManager").GetComponent<LevelManager>();
        orbGunController = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<OrbGunController>();
        _animator = GetComponent<Animator>();
        defaultFollowSpeed = followSpeed;
    }

    private void LateUpdate()
    {
        if(target != null && followTarget)
        {
            FollowTarget();
        }        
    }

    private void FollowTarget()
    {        
        switch (followState)
        {
            case FollowState.player:
                _animator.Play("Moving");
                transform.position = Vector3.Lerp(transform.position, target[0].position, Time.deltaTime * followSpeed);
                followSpeed = defaultFollowSpeed;
                if (orbType == OrbType.blue)
                {
                    if (InputManager.ShootBlueOrbButton())
                    {
                        followState = FollowState.orb;
                    }
                }
                if (orbType == OrbType.pink)
                {
                    if (InputManager.ShootPinkOrbButton())
                    {
                        followState = FollowState.orb;
                    }
                }
                break;
            case FollowState.orb:
                _animator.Play("Moving");
                followSpeed = defaultFollowSpeed * 2f;
                transform.position = Vector3.Lerp(transform.position, target[1].position, Time.deltaTime * followSpeed);
                bool reachedTarget = GetComponent<CircleCollider2D>().IsTouchingLayers(LayerMask.GetMask("Orb"));
                if (reachedTarget)
                {
                    if (orbType == OrbType.blue)
                    {
                        if (!InputManager.ShootBlueOrbButton())
                        {
                            followState = FollowState.player;
                        }                        
                    }
                    if (orbType == OrbType.pink)
                    {
                        if (!InputManager.ShootPinkOrbButton())
                        {
                            followState = FollowState.player;
                        }
                    }
                }
                break;
        }
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if(orbType == OrbType.blue && !levelManager.blueOrbUsable)
            {
                levelManager.blueOrbUsable = true;
            }
            if (orbType == OrbType.pink && !levelManager.pinkOrbUsable)
            {
                levelManager.pinkOrbUsable = true;
            }                
            orbGunController.CrosshairUI(true);
            followTarget = true;
            GetComponent<CircleCollider2D>().radius = 0.5f;
        }
    }
}
