﻿using UnityEngine;

public static class InputManager
{
    private static float deadzone = 0.2f;
    public static float MainHorizontal()
    {
        /*
        #region Joystick deadzone layer controller
        Vector2 inputForce = new Vector2(Input.GetAxisRaw("J_MainHorizontal"), 0f);
        if (inputForce.magnitude < deadzone)
        {
            inputForce = Vector2.zero;
        }
        float j = inputForce.x;
        j = (inputForce.x > 0) ? j : j * 1f;
        #endregion
        */

        float h = 0.0f;
        //h += j;
        //h += Input.GetAxis("J_MainHorizontal");
        h += Input.GetAxis("K_MainHorizontal");
        return Mathf.Clamp(h, -1.0f, 1.0f);
    }

    public static float MainVertical()
    {
        float v = 0.0f;
        //v += Input.GetAxis("J_MainVertical");
        v += Input.GetAxis("K_MainVertical");
        return Mathf.Clamp(v, -1.0f, 1.0f);
    }

    public static Vector2 MainJoystick()
    {
        Vector2 j = new Vector2(MainHorizontal(), MainVertical());
        if(j.magnitude < deadzone)
        {
            j = Vector2.zero;
        }
        return j;
    }

    public static bool SubmitButton()
    {
        bool submit;
        submit =
            Input.GetButtonDown("Submit") ||
            Input.GetButtonDown("Jump");   
        return submit;
    }

    public static bool BackButton()
    {
        bool back;
        back = 
            Input.GetButtonDown("Cancel");
        return back;
    }

    public static bool JumpButton()
    {
        bool jump;
        jump =
            Input.GetButton("Jump");
        return jump;
    }

    public static bool ShootBlueOrbButton()
    {
        bool shootBlue;
        shootBlue =
            Input.GetMouseButton(0) ||
            Input.GetAxis("Right Trigger") > 0.5f ||
            Input.GetButton("Right Bumber");
        return shootBlue;
    }

    public static bool ShootBlueOrbButtonDown()
    {
        bool shootBlue;
        shootBlue =
            Input.GetMouseButtonDown(0) ||
            Input.GetAxis("Right Trigger") > 0f ||
            Input.GetButtonDown("Right Bumber");
        return shootBlue;
    }

    public static bool ShootPinkOrbButton()
    {
        bool shootPink;
        shootPink =
            Input.GetMouseButton(1) ||
            Input.GetAxis("Left Trigger") > 0.5f ||
            Input.GetButton("Left Bumber");
        return shootPink;
    }

    public static bool ShootPinkOrbButtonDown()
    {
        bool shootPink;
        shootPink =
            Input.GetMouseButtonDown(1) ||
            Input.GetAxis("Left Trigger") > 0f ||
            Input.GetButtonDown("Left Bumber");
        return shootPink;
    }
}
