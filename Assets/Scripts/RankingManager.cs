﻿using System.Collections.Generic;
using UnityEngine;

public class RankingManager : MonoBehaviour
{
    private PlayerRatingManager playerRatingManager;
    private TimerManager timerManager;

    public float playerTime;

    public enum Rating
    {
        S,A,B,C,D
    }
    [System.Serializable]
    public struct Rank
    {
        public Rating rating;
        public float timeToBeat;
    }
    public List<Rank> rank;

    private void Start()
    {
        playerRatingManager = GetComponentInChildren<PlayerRatingManager>();
        timerManager = GetComponentInChildren<TimerManager>();
    }

    public void StartLevelTimer()
    {
        timerManager.TimerOn = true;
        StartCoroutine(timerManager.TimerController());        
    }

    public void ResetTimerManager()
    {
        timerManager.ResetTimer();
    }

    public void HideTimer()
    {
        transform.GetChild(0).gameObject.SetActive(false);
    }

    public void DisplayTimer()
    {
        transform.GetChild(0).gameObject.SetActive(true);
    }

    public void ProcessPlayerRating()
    {
        playerRatingManager.DisplayPlayerTime(playerTime);

        if (playerTime >= rank[0].timeToBeat)
        {
            //playerRatingManager.DisplayPlayerRating(rank[0].rating.ToString());
            playerRatingManager.DisplayPlayerRating("A+");
        }
        if (playerTime >= rank[1].timeToBeat)
        {
            playerRatingManager.DisplayPlayerRating(rank[1].rating.ToString());
        }
        if (playerTime >= rank[2].timeToBeat)
        {
            playerRatingManager.DisplayPlayerRating(rank[2].rating.ToString());
        }
        if (playerTime >= rank[3].timeToBeat)
        {
            playerRatingManager.DisplayPlayerRating(rank[3].rating.ToString());
        }
        if (playerTime >= rank[4].timeToBeat)
        {
            playerRatingManager.DisplayPlayerRating(rank[4].rating.ToString());
        }
    }

    public void DisplayEndOfLevelCommands()
    {
        transform.GetChild(2).gameObject.SetActive(true);
    }
}
