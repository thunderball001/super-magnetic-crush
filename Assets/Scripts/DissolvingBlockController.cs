﻿using UnityEngine;
using System.Collections;

public class DissolvingBlockController : MonoBehaviour
{
    [SerializeField] float dissolveSpeed;
    [SerializeField] int health;
    [SerializeField] Sprite[] healthState;
    [SerializeField] GameObject dissolveEffect;
    [SerializeField] AudioClip[] crumbleSound;

    private Animator _animator;
    private SpriteRenderer _spriteRenderer;
    private AudioSource _audioSource;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _audioSource = GetComponentInChildren<AudioSource>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if(Input.GetAxis("Vertical") < 0f)
            {                
                DecreaseHealth();
                if(health <= 0)
                {
                    //_animator.Play("Dissolve");
                    //_animator.speed = dissolveSpeed;
                    StartCoroutine(Destroy());
                }                
            }            
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("BlueOrb") || collision.gameObject.CompareTag("PinkOrb") || collision.gameObject.CompareTag("Hazard"))
        {
            //_animator.Play("Dissolve");
            //_animator.speed = dissolveSpeed;
            DecreaseHealth();
            if (health <= 1)
            {
                //_animator.Play("Dissolve");
                //_animator.speed = dissolveSpeed;
                StartCoroutine(Destroy());
            }
        }
    }

    private void DecreaseHealth()
    {
        health -= 1;
        int newHealthState = health;
        _spriteRenderer.sprite = healthState[newHealthState];
        Instantiate(dissolveEffect, transform.position, Quaternion.identity);
        int randomSound = Random.Range(0, 1);
        _audioSource.PlayOneShot(crumbleSound[randomSound]);
    }

    private void RemoveCollider()
    {
        GetComponent<BoxCollider2D>().enabled = false;
    }

    private IEnumerator Destroy()
    {
        int randomSound = Random.Range(0, 1);
        _audioSource.PlayOneShot(crumbleSound[randomSound]);
        _spriteRenderer.enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
        yield return new WaitForSeconds(2f);
        Destroy(gameObject);
    }

    private IEnumerator DissolveAnimation()
    {        
        while(health > 0)
        {
            DecreaseHealth();
            yield return null;
        }
        Destroy();
    }
}
