﻿using UnityEngine;
using System.Collections;

public class BossController : MonoBehaviour
{
    [SerializeField] float moveSpeed;
    [SerializeField] Transform stopPoint;

    private Animator _animator;

    private void Awake()
    {
        _animator = GetComponentInChildren<Animator>();
    }

    private void Start()
    {
        StartCoroutine(MoveUp()); 
    }

    private IEnumerator MoveUp()
    {
        while(transform.position.y < stopPoint.position.y)
        {
            transform.Translate(Vector2.up * Time.deltaTime * moveSpeed);
            yield return null;
        }
        StartCoroutine(StopMoving());
    }

    private IEnumerator StopMoving()
    {
        _animator.Play("Stop");
        yield return new WaitForSecondsRealtime(2f);
        StartCoroutine(MoveDown());
    }

    private IEnumerator MoveDown()
    {
        while (true)
        {
            transform.Translate(Vector2.down * Time.deltaTime * moveSpeed);
            yield return null;
        }
    }
}
