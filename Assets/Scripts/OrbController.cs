﻿using UnityEngine;

public class OrbController : MonoBehaviour
{
    public bool orbSpawning;
    [SerializeField] bool orbColliding;

    private Animator _animator;
    private AudioSource _audioSource;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _audioSource = GetComponentInChildren<AudioSource>();
    }

    private void LateUpdate()
    {
        if (orbColliding)
        {
            //PlayBounceAnimation();
            //orbColliding = false;
        }
    }

    public void PlaySpawnAnimation()
    {
        _animator.SetTrigger("Spawn");
        _audioSource.Play();
    }

    private void OrbSpawnStart()
    {
        orbSpawning = true;
    }

    private void OrbSpawnEnd()
    {
        orbSpawning = false;
    }

    public void PlayBounceAnimation()
    {
        _animator.SetTrigger("Bouncing");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") || collision.CompareTag("-") || collision.CompareTag("+"))
        {
            if (!orbSpawning)
            {
                //orbColliding = true;
                PlayBounceAnimation();
            }
        }        
    }
}
