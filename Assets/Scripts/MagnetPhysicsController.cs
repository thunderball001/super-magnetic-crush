﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class MagnetPhysicsController : MonoBehaviour
{

    private float fixedTimeAmount;

    [SerializeField] float multiplier;
    [SerializeField] float repelStrength;
    [SerializeField] float attractStrength;
    [SerializeField] float distanceStretch;

    private RigidbodyController rigidbodyController;
    private Rigidbody2D _rb2d;
    private CircleCollider2D _magnetCollider;
    private MagnetCollisionController _magnet;

    [NonSerialized] public List<GameObject> negativeMagnetObjects = new List<GameObject>();
    [NonSerialized] public List<GameObject> positiveMagnetObjects = new List<GameObject>();

    private void Awake()
    {
        fixedTimeAmount = Time.fixedDeltaTime * 50f;
        repelStrength = repelStrength * multiplier * fixedTimeAmount;
        attractStrength = attractStrength * multiplier * fixedTimeAmount;
        distanceStretch = distanceStretch * multiplier * fixedTimeAmount;
        rigidbodyController = GetComponent<RigidbodyController>();
        _rb2d = GetComponent<Rigidbody2D>();
        _magnetCollider = GetComponentInChildren<CircleCollider2D>();
        _magnet = GetComponentInChildren<MagnetCollisionController>();      
    }

    private void FixedUpdate()
    {
        fixedTimeAmount = Time.fixedDeltaTime * 50f; //50 calls per second
        if(_magnet.repelling || _magnet.attracting)
        {
            rigidbodyController.ChangeRigidBodyState(RigidbodyController.RigidBodyState.Magnetised);
        }
        if (_magnet.repelling)
        {
            Repel(fixedTimeAmount);
        }
        if (_magnet.attracting)
        {
            Attract(fixedTimeAmount);
        }

        if (!_magnet.repelling && !_magnet.attracting)
        {
            rigidbodyController.ChangeRigidBodyState(RigidbodyController.RigidBodyState.Default);
        }

    }

    private void Repel(float fixedTimeAmount)
    {
        if (_magnetCollider.IsTouchingLayers(LayerMask.GetMask("-")))
        {
            foreach (GameObject _negMagObj in negativeMagnetObjects)
            {
                Vector2 directionToMagnet = _negMagObj.transform.position - transform.position * fixedTimeAmount;
                float distance = Vector2.Distance(_negMagObj.transform.position, transform.position) * fixedTimeAmount;
                float magnetDistanceStrength = (distanceStretch / distance) * repelStrength * fixedTimeAmount;
                Vector2 force = magnetDistanceStrength * (directionToMagnet * -1) * fixedTimeAmount;                
                _rb2d.AddForce(force * fixedTimeAmount, ForceMode2D.Force);
            }
        }
        if (_magnetCollider.IsTouchingLayers(LayerMask.GetMask("+")))
        {
            foreach (GameObject _posMagObj in positiveMagnetObjects)
            {
                Vector2 directionToMagnet = _posMagObj.transform.position - transform.position * fixedTimeAmount;
                float distance = Vector2.Distance(_posMagObj.transform.position, transform.position) * fixedTimeAmount;
                float magnetDistanceStrength = (distanceStretch / distance) * repelStrength * fixedTimeAmount;
                Vector2 force = magnetDistanceStrength * (directionToMagnet * -1) * fixedTimeAmount;
                _rb2d.AddForce(force * fixedTimeAmount, ForceMode2D.Force);
            }
        }
    }
    
    private void Attract(float fixedTimeAmount)
    {
        if (_magnetCollider.IsTouchingLayers(LayerMask.GetMask("-")))
        {
            foreach (GameObject _negMagObj in negativeMagnetObjects)
            {
                Vector2 directionToMagnet = _negMagObj.transform.position - transform.position * fixedTimeAmount;
                float distance = Vector2.Distance(_negMagObj.transform.position, transform.position) * fixedTimeAmount;
                float magnetDistanceStrength = (distanceStretch / distance) * attractStrength * fixedTimeAmount;
                Vector2 force = magnetDistanceStrength * (directionToMagnet * 1) * fixedTimeAmount;
                _rb2d.AddForce(force * fixedTimeAmount, ForceMode2D.Force);
            }
        }
        if (_magnetCollider.IsTouchingLayers(LayerMask.GetMask("+")))
        {
            foreach (GameObject _posMagObj in positiveMagnetObjects)
            {
                Vector2 directionToMagnet = _posMagObj.transform.position - transform.position * fixedTimeAmount;
                float distance = Vector2.Distance(_posMagObj.transform.position, transform.position) * fixedTimeAmount;
                float magnetDistanceStrength = (distanceStretch / distance) * attractStrength * fixedTimeAmount;
                Vector2 force = magnetDistanceStrength * (directionToMagnet * 1) * fixedTimeAmount;
                _rb2d.AddForce(force * fixedTimeAmount, ForceMode2D.Force);
            }
        }
    }
}
