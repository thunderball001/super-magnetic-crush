﻿using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;
using TMPro;
using UnityEngine.SceneManagement;
using System.Collections;

public class LevelTransitionController : MonoBehaviour
{
    private GotoNextLevelController gotoNextLevelController;

    private Animator _transitionAnimator;

    private ProCamera2DCinematics cinematics;

    private void Awake()
    {        
        DontDestroyOnLoad(gameObject);
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
        HeartTransition(true);
        gotoNextLevelController = GetComponent<GotoNextLevelController>();
        _transitionAnimator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Equals))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        if (Input.GetKey(KeyCode.Minus))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
        }
        if(GameManager.gameState == GameManager.GameState.Menus)
        {
            Destroy(gameObject);
        }
    }

    private void HeartTransition(bool state)
    {
        GameObject heartTransition;
        heartTransition = transform.GetChild(0).gameObject;
        heartTransition.SetActive(state);
    }

    private void DisplayLevelNameText(string text)
    {
        TextMeshProUGUI levelNameText;
        levelNameText = transform.GetChild(1).transform.GetChild(0).gameObject.GetComponent<TextMeshProUGUI>();

        levelNameText.text = text;
    }

    public void PlayerEnterAnimation()
    {
        _transitionAnimator.Play("Heart Transition Enter");        
    }

    public void PlayIntroAnimation()
    {
        HeartTransition(false);        
        _transitionAnimator.Play("LevelIntro");
        DisplayLevelNameText(SceneManager.GetActiveScene().name);
    }

    public void PlayerOutroAnimation()
    {        
        _transitionAnimator.Play("LevelOutro");
    }

    public void PlayExitAnimation()
    {
        HeartTransition(true);
        //ProCamera2DTransitionsFX.Instance.TransitionExit();
        _transitionAnimator.Play("Heart Transition Exit");
    }

    public void EnableGotoNextLevelController()
    {        
        gotoNextLevelController.enabled = true;
    }
}
