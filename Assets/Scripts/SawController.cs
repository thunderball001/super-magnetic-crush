﻿using UnityEngine;
using System.Collections;

public class SawController : MonoBehaviour
{
    [SerializeField] private enum SawState
    {
        Static,
        Moving,
        Shot
    }
    [SerializeField] private SawState sawState;

    [SerializeField] float rotateSpeed;
    [SerializeField] Vector2 shotDirection;
    [SerializeField] float respawnTime;
    [SerializeField] Transform[] target;
    private bool targetSwitch;

    private Vector2 spawnPoint;    

    private Rigidbody2D _rb2d;

    private void Awake()
    {
        transform.position = target[0].position;
        spawnPoint = transform.position;
        _rb2d = GetComponent<Rigidbody2D>();
    }

    private void Start()
    {      
        ManageSawStates();
    }

    private void FixedUpdate()
    {
        SpinSaw();
        //ManageSawStates();
    }

    private void ManageSawStates()
    {
        switch (sawState)
        {
            case SawState.Static:

                break;
            case SawState.Moving:
                StartCoroutine(MoveSawAlongPath(true));               
                break;
            case SawState.Shot:
                ShootSaw();
                break;
        }
    }

    private void SpinSaw()
    {
        _rb2d.MoveRotation(_rb2d.rotation + rotateSpeed * Time.fixedDeltaTime);
    }

    private IEnumerator MoveSawAlongPath(bool targetSwitch)
    {        
        while(sawState == SawState.Moving)
        {
            if (transform.position == target[1].position)
            {
                targetSwitch = true;
            }
            if (transform.position == target[0].position)
            {
                targetSwitch = false;
            }
            if (targetSwitch)
            {
                _rb2d.MovePosition(Vector2.Lerp(transform.position, target[0].position, 5f * Time.fixedDeltaTime));
            }
            if (!targetSwitch)
            {
                _rb2d.MovePosition(Vector2.Lerp(transform.position, target[1].position, 5f * Time.fixedDeltaTime));
            }
            yield return null;
        }
    }

    private void ShootSaw()
    {
        _rb2d.velocity = shotDirection;
    }

    private IEnumerator Respawn()
    {
        transform.position = spawnPoint;
        _rb2d.velocity = new Vector2(0f, 0f);
        yield return new WaitForSeconds(respawnTime);        
        ManageSawStates();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(sawState == SawState.Shot)
        {
            if (collision.gameObject.CompareTag("Ground"))
            {
                StartCoroutine(Respawn());
            }
        }                   
    }
}
