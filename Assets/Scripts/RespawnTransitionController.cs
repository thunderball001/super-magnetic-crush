﻿using UnityEngine;
using Com.LuisPedroFonseca.ProCamera2D;

public class RespawnTransitionController : MonoBehaviour
{
    private void Start()
    {
        ProCamera2DTransitionsFX.Instance.TransitionEnter();
    }
}
