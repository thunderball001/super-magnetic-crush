﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovingOrbsManager : MonoBehaviour
{
    [SerializeField] List<Transform> orbSpawnPositions;

    [SerializeField] Transform orb;

    int i; //currentOrbSpawnPosition

    [SerializeField] float timeTilSpawnRotation;

    private void Awake()
    {
        orbSpawnPositions[0].position = orb.position;
    }

    private void Start()
    {
        i = 0;        
        StartCoroutine(RotatePositions());
    }

    private IEnumerator RotatePositions()
    {
        if (i == orbSpawnPositions.Count - 1)
        {
            i = 0;
        }
        else
        {
            i += 1;
        }
        
        yield return new WaitForSeconds(timeTilSpawnRotation);
        orb.position = orbSpawnPositions[i].position;
        orb.GetComponent<OrbController>().PlaySpawnAnimation();
        StartCoroutine(RotatePositions());
    }
}
