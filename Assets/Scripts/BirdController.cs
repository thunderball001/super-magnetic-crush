﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdController : MonoBehaviour
{
    [SerializeField] int flyAwaySpeed;
    [SerializeField] int flyBackSpeed;

    [SerializeField]
    private enum BirdState
    {
        Idle,
        FlyingAway,
        FlyingBack
    }
    [SerializeField] private BirdState birdState;

    private Transform player;
    private Vector2 spawnPosition;

    private Animator _animator;
    private AudioSource _audioSource;

    private void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        spawnPosition = transform.position;
        _animator = GetComponent<Animator>();
        _audioSource = GetComponentInChildren<AudioSource>();
        FlipSpriteRandomly();         
    }

    private void FlipSpriteRandomly()
    {
        int randomNumber = Random.Range(0, 2);
        if (randomNumber == 0)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("-") || collision.CompareTag("+"))
        {
            _audioSource.Play();
            birdState = BirdState.FlyingAway;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("-") || collision.CompareTag("+"))
        {
            StartCoroutine(FlyBackController());
        }
    }

    private IEnumerator FlyBackController()
    {
        yield return new WaitForSeconds(2f);
        birdState = BirdState.FlyingBack;
    }

    private void LateUpdate()
    {
        switch (birdState)
        {
            case BirdState.Idle:
                _animator.Play("Bird Idle");
                _animator.speed = 0.7f;
                break;
            case BirdState.FlyingAway:                
                Vector2 direction = (transform.position - player.position).normalized; 
                transform.Translate(direction * flyAwaySpeed * Time.deltaTime);
                _animator.Play("Bird Flying");
                _animator.speed = 1f;
                break;
            case BirdState.FlyingBack:
                transform.position = Vector2.MoveTowards(transform.position, spawnPosition, flyBackSpeed * Time.deltaTime);
                _animator.Play("Bird Flying");
                _animator.speed = 0.7f;
                if (transform.position.x == spawnPosition.x && transform.position.y == spawnPosition.y)
                {
                    FlipSpriteRandomly();
                    birdState = BirdState.Idle;                    
                }
                break;
        }
    }
}
