﻿using UnityEngine;
using TMPro;
using System.Collections;

public class TimerManager : MonoBehaviour
{
    public bool TimerOn;
    private RankingManager rankingManager;

    private float playerTimer;
    private TextMeshProUGUI playerTimerText;

    private void Start()
    {
        rankingManager = GameObject.FindGameObjectWithTag("RankingManager").GetComponent<RankingManager>();
        playerTimerText = GetComponentInChildren<TextMeshProUGUI>();
        playerTimerText.text = "0.00";        
    }

    public IEnumerator TimerController()
    {
        while (playerTimer <= 99f && TimerOn)
        {
            rankingManager.playerTime = playerTimer;
            playerTimer += Time.deltaTime;
            playerTimerText.text = playerTimer.ToString("F2");            
            yield return null;
        }
    }

    public void ResetTimer()
    {
        playerTimer = 0;
    }
}
