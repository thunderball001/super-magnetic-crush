﻿using UnityEngine;
using TMPro;

public class PlayerRatingManager : MonoBehaviour
{

    private GameObject playerRatingBox;
    private TextMeshProUGUI playerRatingText;
    private TextMeshProUGUI playerTimeText;

    private void Awake()
    {
        transform.GetChild(0).gameObject.SetActive(true);
    }

    private void Start()
    {
        playerRatingBox = transform.GetChild(0).gameObject;
        playerRatingText = transform.GetChild(1).GetComponent<TextMeshProUGUI>();
        playerTimeText = transform.GetChild(2).GetComponent<TextMeshProUGUI>();
        playerRatingBox.SetActive(false);
        playerRatingText.gameObject.SetActive(false);
        playerTimeText.gameObject.SetActive(false);
    }

    public void DisplayPlayerRating(string playerRating)
    {
        playerRatingBox.SetActive(true);
        playerRatingText.gameObject.SetActive(true);
        playerRatingText.text = playerRating;
    }

    public void DisplayPlayerTime(float playerTime)
    {
        playerTimeText.gameObject.SetActive(true);
        playerTimeText.text = playerTime.ToString("F2");
    }

    public void RemovePlayerRating()
    {
        playerRatingText.text = "";
        playerTimeText.text = "";
    }
}
