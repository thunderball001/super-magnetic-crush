﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseScreenManager : MonoBehaviour
{

    public static bool paused;
    public static bool resume;

    public static bool timeOut;

    private bool controlsScreenON;

    [SerializeField] GameObject pauseScreen;
    [SerializeField] GameObject controlsScreen;

    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        resume = true;
        timeOut = false;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            if (paused)
            {
                if (!controlsScreenON)
                {
                    Resume();
                }
                else
                {
                    BackButton();
                }
                
            }
            else
            {
                Pause();
            }            
        }
        else
        {
            if (timeOut && !paused)
            {
                Pause();
            }
        }        
    }

    public void Resume()
    {
        timeOut = false;
        pauseScreen.SetActive(false);
        controlsScreen.SetActive(false);
        _audioSource.Stop();
        Time.timeScale = 1f;
        Cursor.visible = false;
        paused = false;
    }

    private void Pause()
    {
        timeOut = false;
        pauseScreen.SetActive(true);
        _audioSource.Play();
        Time.timeScale = 0f;
        Cursor.visible = true;
        paused = true;
    }

    public void GotoLevelSelectMenu()
    {
        resume = false;
        paused = false;
        timeOut = false;
        Time.timeScale = 1f;
        SceneManager.LoadScene(2);
    }

    public void ControlsScreen()
    {
        controlsScreen.SetActive(true);
        controlsScreenON = true;
    }

    public void BackButton()
    {
        controlsScreen.SetActive(false);
        controlsScreenON = false;
    }

    public void QuitGame()
    {
        //Application.Quit();
    }

    public static void RestartGame()
    {
        resume = false;
        paused = false;
        timeOut = false;
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }
}
